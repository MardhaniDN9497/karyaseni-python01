# Karya Seni sederhana dengan python  

Berikut script sederhana dengan extensi.py

```python
import turtle #Memanggil turtle dari library
turtle.title("Pergerakan Turtle") #Memberi judul pada grafik 
turtle.resizemode("small") #Mengubah ukuran turtle
turtle.color("black") #Mengubah warna turtle
turtle.shape("turtle") #Menunjukkan bentuk turtle
turtle.speed(100) #Mengatur kecepatan gerak turtle
t=turtle #Menyimpan turtle dalam variabel t

#Proses Pergerakan
for i in range(1,73) : #Digunakan untuk perulangan dari 1 sampai 72
    if i%1==0 : #Program bekerja jika kondisi i mod 1 = 0
        for i in range(4): #Digunakan untuk perulangan dari 1 sampai 4
            t.forward(100) #Turtle bergerak ke depan sebesar 100 satuan
            t.right(90) #Turtle berputar ke kanan sebesar 90 derajat
        for i in range(1): 
            t.right(5) #Turtle berputar ke kanan sebesar 5 derajat
```
